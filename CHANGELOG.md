# Sobelow SAST analyzer changelog

## v2.14.0
- Enable vulnerabilities in `Config` module (!67 @rbf)

## v2.13.0
- Add URL references to the Sobelow moduledocs from where the vulnerability
  descriptions have been extracted (!65 @rbf)
- Make all vulnerability descriptions consistent with the source referenced (!65 @rbf)

## v2.12.2
- Fix vulnerabilities found by gosec (!62)

## v2.12.1
- Update Dockerfile to restrict sobelow permissions (!54)

## v2.12.0
- Update Dockerfile to support openshift (!53)

## v2.11.0
- Update `report` dependency (!52)
  - Update schema version to `14.0.0`

## v2.10.0
- Update sobelow to v0.11.1 (!50)
  - Bugfix: Command Injection finding description are properly formatted
  - Misc: Custom JSON serialization replaced with Jason.

## v2.9.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!49)

## v2.9.0
- Update common to v2.22.0 (!48)
- Update urfave/cli to v2.3.0 (!48)

## v2.8.0
- Update elixir elixir to v1.11 (!45)
- Update sobelow to v0.10.6
- update logrus and cli golang dependencies to latest versions

## v2.7.2
- Update common and enabled disablement of rulesets (!44)

## v2.7.1
- Update golang dependencies (!38)

## v2.7.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!35)

## v2.6.1
- Upgrade go to version 1.15 (!34)

## v2.6.0
- Add scan object to report (!30)

## v2.5.1
- Bump sobelow to [v0.10.4](https://github.com/nccgroup/sobelow/blob/master/CHANGELOG.md#v0104) (!28)
- Bump elixir base image to 1.10.4

## v2.5.0
- Switch to the MIT Expat license (!26)

## v2.4.1
- Update Debug output to give a better description of command that was ran (!25)

## v2.4.0
- Update logging to be standardized across analyzers (!24)

## v2.3.4
- Bump sobelow to v0.10.3 (!23)

## v2.3.3
- Remove `location.dependency` from the generated SAST report (!22)

## v2.3.2
- Upgrade elixir base image to 1.10.3 (!21)
- Upgrade to [Sobelow 0.10.2](https://github.com/nccgroup/sobelow/blob/master/CHANGELOG.md#v0102)

## v2.3.1
- Use Alpine as builder image (!17)

## v2.3.0
- Add `id` field to vulnerabilities in JSON report (!16)

## v2.2.0
- Add support for custom CA certs (!14)

## v2.1.1
- Upgrade elixir base image to 1.9.4

## v2.1.0
- Upgrade to [Sobelow 0.8.0](https://github.com/nccgroup/sobelow/blob/master/CHANGELOG.md#v080)
- Fix link to affected line of the source code
- Change compare keys, use fixed line number and remove function name

## v2.0.1
- Define MIX_HOME in Dockerfile to prevent dynamic definition at runtime

## v2.0.0
- Initial release
