package convert

import (
	"encoding/json"
	"fmt"
	"io"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2/metadata"
)

// Convert reads a native report from the tool (sobelow) and transforms it
// into issues (vulnerabilities), as defined in the common module
func Convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var fileReport SoBelowReport

	err := json.NewDecoder(reader).Decode(&fileReport)
	if err != nil {
		return nil, err
	}

	// Create vulns from sobelow report
	vulns := make([]report.Vulnerability, 0)

	for _, vuln := range fileReport.vulnerabilities() {
		description, ok := vuln.description()
		if !ok {
			// Ignore unknown rule types
			continue
		}

		newVuln := report.Vulnerability{
			Category:    metadata.Type,
			Scanner:     metadata.IssueScanner,
			Name:        vuln.message(),
			Message:     vuln.message(),
			Description: description,
			CompareKey:  vuln.compareKey(),
			Severity:    report.SeverityLevelUnknown,
			Confidence:  vuln.Confidence,
			Location: report.Location{
				File:      vuln.File,
				LineStart: vuln.Line,
			},
			Identifiers: []report.Identifier{
				{
					Type:  "sobelow_rule_id",
					Name:  fmt.Sprintf("Sobelow Rule ID %s", vuln.typeSlug()),
					Value: vuln.typeSlug(),
				},
			},
		}

		vulns = append(vulns, newVuln)
	}

	newReport := report.NewReport()
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = vulns
	return &newReport, nil
}
