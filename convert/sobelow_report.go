package convert

import (
	"strconv"
	"strings"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

// SoBelowReport contains information about the sobelow scan and the findings
type SoBelowReport struct {
	Findings       SoBelowConfidenceGroups `json:"findings"`
	SoBelowVersion string                  `json:"sobelow_version"`
	TotalFindings  int                     `json:"total_findings"`
}

// SoBelowConfidenceGroups contains a collection of vulnerabilities arranged by confidence
type SoBelowConfidenceGroups struct {
	HighConfidence   []SoBelowVulnerability `json:"high_confidence"`
	MediumConfidence []SoBelowVulnerability `json:"medium_confidence"`
	LowConfidence    []SoBelowVulnerability `json:"low_confidence"`
}

// SoBelowVulnerability is a sobelow vulnerability
type SoBelowVulnerability struct {
	Type       string
	File       string
	Line       int
	Confidence report.ConfidenceLevel
}

func (r *SoBelowReport) vulnerabilities() []SoBelowVulnerability {
	vulnerabilityCount := len(r.Findings.HighConfidence) + len(r.Findings.MediumConfidence) + len(r.Findings.LowConfidence)
	vulnerabilities := make([]SoBelowVulnerability, vulnerabilityCount)

	for _, vulnerability := range r.Findings.HighConfidence {
		vulnerability.Confidence = report.ConfidenceLevelHigh
		vulnerabilities = append(vulnerabilities, vulnerability)
	}
	for _, vulnerability := range r.Findings.MediumConfidence {
		vulnerability.Confidence = report.ConfidenceLevelMedium
		vulnerabilities = append(vulnerabilities, vulnerability)
	}
	for _, vulnerability := range r.Findings.LowConfidence {
		vulnerability.Confidence = report.ConfidenceLevelLow
		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	return vulnerabilities
}

// compareKey returns a string used to establish whether two issues are the same.
func (v *SoBelowVulnerability) compareKey() string {
	attrs := []string{v.File, strconv.Itoa(v.Line), v.typeSlug()}
	return strings.Join(attrs, ":")
}

func (v *SoBelowVulnerability) typeSlug() string {
	return typeSlugs[v.Type]
}

func (v *SoBelowVulnerability) message() string {
	withoutType := strings.Split(v.Type, ": ")[1:]
	return strings.Join(withoutType, "")
}

// description returns a description if type is supported
func (v *SoBelowVulnerability) description() (string, bool) {
	desc, ok := descriptions[v.typeSlug()]
	return desc, ok
}

// NOTE: The URLs below use the old 'v0.10.0' tag, although it's not the version
// used in the project (see Dockerfile) because there is currently no tag for
// that version on the sobelow repo (see
// https://github.com/nccgroup/sobelow/tags). For more context see:
//   - https://gitlab.com/gitlab-org/security-products/analyzers/sobelow/-/merge_requests/65#note_707121892
//   - https://github.com/nccgroup/sobelow/issues/102

var typeSlugs = map[string]string{
	// Command Injection
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/ci
	"CI.OS: Command Injection in `:os.cmd`":        "ci_in_os_cmd",
	"CI.System: Command Injection in `System.cmd`": "ci_in_system_cmd",

	// Config
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/config
	"Config.CSP: Missing Content-Security-Policy":      "config_missing_content_security_policy",
	"Config.CSRF: Missing CSRF Protections":            "config_missing_csrf_protections",
	"Config.CSRFRoute: CSRF via Action Reuse":          "config_csrf_via_action_reuse",
	"Config.CSWH: Cross-Site Websocket Hijacking":      "config_cross_site_websocket_hijacking",
	"Config.Headers: Missing Secure Browser Headers":   "config_missing_secure_browser_headers",
	"Config.HSTS: HSTS Not Enabled":                    "config_hsts_not_enabled",
	"Config.HTTPS: HTTPS Not Enabled":                  "config_https_not_enabled",
	// Disabled Config.Secrets detection as it would surface duplicate issues as
	// GitLab's own Secret Detection layer.
	// https://docs.gitlab.com/ee/user/application_security/secret_detection/
	// "Config.Secrets: Hardcoded Secret":                 "config_hardcoded_secret",

	// Denial of Service
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/dos
	"DOS.ListToAtom: Unsafe `List.to_atom`":     "dos_unsafe_list_to_atom",
	"DOS.StringToAtom: Unsafe `String.to_atom`": "dos_unsafe_string_to_atom",
	"DOS.BinToAtom: Unsafe atom interpolation":  "dos_unsafe_atom_interpolation",

	// Misc
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/misc
	"Misc.BinToTerm: Unsafe `binary_to_term`": "misc_unsafe_binary_to_term",

	// RCE
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/rce
	"RCE.EEx: Code Execution in `EEx.eval_file`":           "rce_in_eex_eval_file",
	"RCE.EEx: Code Execution in `EEx.eval_string`":         "rce_in_eex_eval_string",
	"RCE.CodeModule: Code Execution in `Code.eval_file`":   "rce_in_code_eval_file",
	"RCE.CodeModule: Code Execution in `Code.eval_quoted`": "rce_in_code_eval_quoted",
	"RCE.CodeModule: Code Execution in `Code.eval_string`": "rce_in_code_eval_string",

	// SQL
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/sql
	"SQL.Query: SQL injection":  "sql_injection",
	"SQL.Stream: SQL injection": "sql_injection",

	// Traversal
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/traversal
	"Traversal.SendDownload: Directory Traversal in `send_download`": "traversal_in_send_download",
	"Traversal.SendFile: Directory Traversal in `send_file`":         "traversal_in_send_file",
	"Traversal.FileModule: Directory Traversal in `File.cp!`":        "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.cp_r!`":      "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.cp_r`":       "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.cp`":         "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.ln!`":        "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.ln_s!`":      "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.ln_s`":       "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.ln`":         "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.read!`":      "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.read`":       "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.rm!`":        "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.rm_rf`":      "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.rm`":         "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.write!`":     "traversal_in_file_module",
	"Traversal.FileModule: Directory Traversal in `File.write`":      "traversal_in_file_module",

	// XSS
	// https://github.com/nccgroup/sobelow/tree/v0.10.0/lib/sobelow/xss
	"XSS.Raw: XSS":                                    "xss",
	"XSS.HTML: XSS in `html`":                         "xss_in_html",
	"XSS.SendResp: XSS in `send_resp`":                "xss",
	"XSS.ContentType: XSS in `put_resp_content_type`": "xss_in_put_resp_content_type",
}

// These descriptions are taken from Sobelow's moduledocs (see URLs inline below)
var descriptions = map[string]string{

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/ci.ex#L3-L10
	"ci_in_os_cmd":                           "Command Injection: Command Injection vulnerabilities are a result of passing untrusted input to an operating system shell, and may result in complete system compromise. Read more about Command Injection here: https://www.owasp.org/index.php/Command_Injection",
	"ci_in_system_cmd":                       "Command Injection: Command Injection vulnerabilities are a result of passing untrusted input to an operating system shell, and may result in complete system compromise. Read more about Command Injection here: https://www.owasp.org/index.php/Command_Injection",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/csp.ex#L3-L18
	"config_missing_content_security_policy": "Missing Content-Security-Policy: Content-Security-Policy is an HTTP header that helps mitigate a number of attacks, including Cross-Site Scripting. Read more about CSP here: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP. Missing Content-Security-Policy is flagged by `sobelow` when a pipeline implements the `:put_secure_browser_headers` plug, but does not provide a Content-Security-Policy header in the custom headers map. Documentation on the `put_secure_browser_headers` plug function can be found here: https://hexdocs.pm/phoenix/Phoenix.Controller.html#put_secure_browser_headers/2",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/csrf.ex#L3-L14
	"config_missing_csrf_protections":        "Cross-Site Request Forgery: In a Cross-Site Request Forgery (CSRF) attack, an untrusted application can cause a user's browser to submit requests or perform actions on the user's behalf. Read more about CSRF here: https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF). Cross-Site Request Forgery is flagged by `sobelow` when a pipeline fetches a session, but does not implement the `:protect_from_forgery` plug.",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/csrf_route.ex#L3-L19
	"config_csrf_via_action_reuse":           "Cross-Site Request Forgery: In a Cross-Site Request Forgery (CSRF) attack, an untrusted application can cause a user's browser to submit requests or perform actions on the user's behalf. Read more about CSRF here: https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF). This type of CSRF is flagged by `sobelow` when state-changing routes share an action with GET-based routes. For example: `get " + `"/users"` + ", UserController, :new` and ` post " + `"/users"` + ", UserController, :new`. In this instance, it may be possible to trigger the POST functionality with a GET request and query parameters.",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/cswh.ex#L3-L9
	"config_cross_site_websocket_hijacking":  "Cross-Site Websocket Hijacking: Websocket connections are not bound by the same-origin policy. Connections that do not validate the origin may leak information to an attacker. More information can be found here: https://www.christian-schneider.net/CrossSiteWebSocketHijacking.html",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/headers.ex#L3-L11
	"config_missing_secure_browser_headers":  "Missing Secure HTTP Headers: By default, Phoenix HTTP responses contain a number of secure HTTP headers that attempt to mitigate XSS, click-jacking, and content-sniffing attacks. Missing Secure HTTP Headers is flagged by `sobelow` when a pipeline accepts " + `"html"` + " requests, but does not implement the `:put_secure_browser_headers` plug.",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/hsts.ex#L3-L7
	"config_hsts_not_enabled":                "HSTS: The HTTP Strict Transport Security (HSTS) header helps defend against man-in-the-middle attacks by preventing unencrypted connections.",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/https.ex#L3-L9
	"config_https_not_enabled":               "HTTPS: Without HTTPS, attackers in a priveleged network position can intercept and modify traffic. Sobelow detects missing HTTPS by checking the prod configuration.",
	// Disabled Config.Secrets detection as it would surface duplicate issues as
	// GitLab's own Secret Detection layer.
	// https://docs.gitlab.com/ee/user/application_security/secret_detection/
	// // https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/config/secrets.ex#L3-L11
	// "config_hardcoded_secret":                "Hard-coded Secrets: In the event of a source-code disclosure via file read vulnerability, accidental commit, etc, hard-coded secrets may be exposed to an attacker. This may result in database access, cookie forgery, and other issues. Sobelow detects missing hard-coded secrets by checking the prod configuration.",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/dos/binary_to_atom.ex#L3-L8
	"dos_unsafe_atom_interpolation":          "Denial of Service via Unsafe Atom Interpolation: In Elixir, atoms are not garbage collected. As such, if user input is used to create atoms (as in " + `:"foo\#{bar}"` + ", or in `:erlang.binary_to_atom`), it may result in memory exhaustion. Prefer the `String.to_existing_atom` function for untrusted user input.",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/dos/list_to_atom.ex#L3-L8
	"dos_unsafe_list_to_atom":                "Denial of Service via `List.to_atom`: In Elixir, atoms are not garbage collected. As such, if user input is passed to the `List.to_atom` function, it may result in memory exhaustion. Prefer the `List.to_existing_atom` function for untrusted user input.",
	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/dos/string_to_atom.ex#L3-L8
	"dos_unsafe_string_to_atom":              "Denial of Service via `String.to_atom`: In Elixir, atoms are not garbage collected. As such, if user input is passed to the `String.to_atom` function, it may result in memory exhaustion. Prefer the `String.to_existing_atom` function for untrusted user input.",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/misc/bin_to_term.ex#L3-L8
	"misc_unsafe_binary_to_term":             "Insecure use of `binary_to_term`: If user input is passed to Erlang's `binary_to_term` function it may result in memory exhaustion or code execution. Even with the `:safe` option, `binary_to_term` will deserialize functions, and shouldn't be considered safe to use with untrusted input.",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/rce/eex.ex#L3-L7
	"rce_in_eex_eval_file":                   "Insecure EEx evaluation: If user input is passed to EEx eval functions, it may result in arbitrary code execution. The root cause of these issues is often directory traversal.",
	"rce_in_eex_eval_string":                 "Insecure EEx evaluation: If user input is passed to EEx eval functions, it may result in arbitrary code execution. The root cause of these issues is often directory traversal.",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/rce.ex#L3-L7
	"rce_in_code_eval_file":                  "Remote Code Execution: Remote Code Execution vulnerabilities are a result of untrusted user input being executed or interpreted by the system and may result in complete system compromise.",
	"rce_in_code_eval_quoted":                "Remote Code Execution: Remote Code Execution vulnerabilities are a result of untrusted user input being executed or interpreted by the system and may result in complete system compromise.",
	"rce_in_code_eval_string":                "Remote Code Execution: Remote Code Execution vulnerabilities are a result of untrusted user input being executed or interpreted by the system and may result in complete system compromise.",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/sql.ex#L3-L11
	"sql_injection":                          "SQL Injection: SQL injection occurs when untrusted input is interpolated directly into a SQL query. In a typical Phoenix application, this would mean using the `Ecto.Adapters.SQL.query` method and not using the parameterization feature. Read more about SQL injection here: https://www.owasp.org/index.php/SQL_Injection",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/traversal.ex#L3-L11
	"traversal_in_send_download":             "Path Traversal: Path traversal vulnerabilities are a result of interacting with the filesystem using untrusted input. This class of vulnerability may result in file disclosure, code execution, denial of service, and other issues. Read more about Path Traversal here: https://www.owasp.org/index.php/Path_Traversal",
	"traversal_in_send_file":                 "Path Traversal: Path traversal vulnerabilities are a result of interacting with the filesystem using untrusted input. This class of vulnerability may result in file disclosure, code execution, denial of service, and other issues. Read more about Path Traversal here: https://www.owasp.org/index.php/Path_Traversal",
	"traversal_in_file_module":               "Path Traversal: Path traversal vulnerabilities are a result of interacting with the filesystem using untrusted input. This class of vulnerability may result in file disclosure, code execution, denial of service, and other issues. Read more about Path Traversal here: https://www.owasp.org/index.php/Path_Traversal",

	// https://github.com/nccgroup/sobelow/blob/v0.10.0/lib/sobelow/xss.ex#L3-L11
	"xss":                                    "Cross-Site Scripting: Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
	"xss_in_html":                            "Cross-Site Scripting: Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
	"xss_in_put_resp_content_type":           "Cross-Site Scripting: Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
}
