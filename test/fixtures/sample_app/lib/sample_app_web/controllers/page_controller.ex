defmodule SampleAppWeb.PageController do
  use SampleAppWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  # XSS
  def xss(conn, %{"unsafe_param" => unsafe_param}) do
    put_resp_content_type(conn, unsafe_param)
    |> send_file(200, "file.txt")
  end

  # SQL injection
  def sql(conn, %{"sql" => sql} = params) do
    Repo.query(sql)

    conn
    |> put_status(:created)
  end

  # Remote code execution - eex
  def rce_eex(conn, %{"unsafe_param" => unsafe_param}) do
    EEx.eval_string(unsafe_param)

    conn
    |> put_status(:ok)
  end

  # Denial of Service due to symbol bombing
  def dos_atom_bomb(conn,  %{"unsafe_template" => unsafe_template}) do
    render conn, String.to_atom(unsafe_template)
  end

  # Remote code execution - os
  def rce_os(conn,  %{"unsafe_cmd" => unsafe_cmd}) do
    :os.cmd(unsafe_cmd)

    conn
    |> put_status(:ok)
  end
end
