package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	ignoredModules = "Vuln" // disable dependency_scanning module
	pathOutput     = "sobelow.json"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		return cmd
	}

	args := []string{
		"sobelow",
		"--strict",  // cause sobelow to exit if it encounters a bad ast
		"--private", // disable outbound version check
		"--format", "json",
		"--ignore", ignoredModules,
		"--out", pathOutput, // set output file
	}

	cmd := setupCmd(exec.Command("mix", args...))
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	return os.Open(filepath.Clean(filepath.Join(path, pathOutput)))
}
